
# CM_21_GLB

## Should use QCrad for pre processing

- Latest QCrad can help to clean data form erroneous records.
- Also use `inspect_days_Lap_sirena.R`.
- Some data exclusions have been implemented on the TOT files without other 
indications.


## Implement a data history columns 

Similar to GC

```
-------------
S------------
S1-----------
S12-1-Q--C---
```

## Add calibration events

Mark calibration events on the plots

## Suspects

1995-08-04....
1995-08-31....
1995-10-16....
1995-11-03....
1995-11-08....
1995-12-26...
1996-01-02...


2012-05-19 wrong calibration
2012-09-20 ..



2022-03-01 GLB / TOT ?
2022-03-02 GLB / TOT ?
2022-03-03 GLB / TOT ?

